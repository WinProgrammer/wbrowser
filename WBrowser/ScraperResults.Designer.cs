﻿namespace WBrowser
{
	partial class ScraperResults
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvwResults = new System.Windows.Forms.ListView();
			this.chText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chFQU = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chCheckBox = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.pnlTop = new System.Windows.Forms.Panel();
			this.btnTest = new System.Windows.Forms.Button();
			this.pnlTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// lvwResults
			// 
			this.lvwResults.CheckBoxes = true;
			this.lvwResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCheckBox,
            this.chText,
            this.chData,
            this.chFQU});
			this.lvwResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvwResults.FullRowSelect = true;
			this.lvwResults.HideSelection = false;
			this.lvwResults.Location = new System.Drawing.Point(0, 43);
			this.lvwResults.Name = "lvwResults";
			this.lvwResults.OwnerDraw = true;
			this.lvwResults.Size = new System.Drawing.Size(959, 563);
			this.lvwResults.TabIndex = 1;
			this.lvwResults.UseCompatibleStateImageBehavior = false;
			this.lvwResults.View = System.Windows.Forms.View.Details;
			this.lvwResults.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwResults_ColumnClick);
			this.lvwResults.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvwResults_DrawColumnHeader);
			this.lvwResults.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvwResults_DrawItem);
			this.lvwResults.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.lvwResults_DrawSubItem);
			// 
			// chText
			// 
			this.chText.Text = "Text";
			this.chText.Width = 202;
			// 
			// chData
			// 
			this.chData.Text = "Data";
			this.chData.Width = 406;
			// 
			// chFQU
			// 
			this.chFQU.Text = "Full Link";
			this.chFQU.Width = 322;
			// 
			// chCheckBox
			// 
			this.chCheckBox.Text = "";
			// 
			// pnlTop
			// 
			this.pnlTop.Controls.Add(this.btnTest);
			this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlTop.Location = new System.Drawing.Point(0, 0);
			this.pnlTop.Name = "pnlTop";
			this.pnlTop.Size = new System.Drawing.Size(959, 43);
			this.pnlTop.TabIndex = 2;
			// 
			// btnTest
			// 
			this.btnTest.Location = new System.Drawing.Point(13, 13);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(75, 23);
			this.btnTest.TabIndex = 0;
			this.btnTest.Text = "Test";
			this.btnTest.UseVisualStyleBackColor = true;
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// ScraperResults
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(959, 606);
			this.Controls.Add(this.lvwResults);
			this.Controls.Add(this.pnlTop);
			this.Name = "ScraperResults";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Scraper Results of";
			this.Load += new System.EventHandler(this.ScraperResults_Load);
			this.pnlTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView lvwResults;
		private System.Windows.Forms.ColumnHeader chText;
		private System.Windows.Forms.ColumnHeader chData;
		private System.Windows.Forms.ColumnHeader chFQU;
		private System.Windows.Forms.ColumnHeader chCheckBox;
		private System.Windows.Forms.Panel pnlTop;
		private System.Windows.Forms.Button btnTest;
	}
}