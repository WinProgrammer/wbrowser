﻿// Goga Claudia
//WBrowser 2009
//Email : goga.claudia@gmail.com
using System;
using System.Windows.Forms;

namespace WBrowser
{
	public partial class AddFavorites : Form
	{
		String url;

		/// <summary>
		/// 
		/// </summary>
		public String favName;

		/// <summary>
		/// 
		/// </summary>
		public String favFile;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="url"></param>
		public AddFavorites(String url)
		{
			this.url = url;
			InitializeComponent();
		}

		private void AddFavorites_Load(object sender, EventArgs e)
		{
			textBox3.Text = url;
			comboBox1.Text = comboBox1.Items[0].ToString();

		}

		private void button1_Click(object sender, EventArgs e)
		{
			favName = textBox3.Text;
			favFile = comboBox1.SelectedItem.ToString();
		}


	}
}
