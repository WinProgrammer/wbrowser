﻿//Goga Claudia
//WBrowser 2009
//Email : goga.claudia@gmail.com
using System;
using System.Windows.Forms;

namespace WBrowser
{
	public partial class RenameLink : Form
	{
		String oldName;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oldName"></param>
		public RenameLink(string oldName)
		{
			this.oldName = oldName;
			InitializeComponent();
		}

		private void RenameLink_Load(object sender, EventArgs e)
		{
			newName.Text = oldName;
		}
	}
}
