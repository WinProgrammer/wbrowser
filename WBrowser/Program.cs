﻿using System;
using System.Windows.Forms;

namespace WBrowser
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			// enable global error handler
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
			// enable global error handler for the MAIN thread (UI thread)
			Application.ThreadException += Application_ThreadException;

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new WBrowser());
		}



		/// <summary>
		/// Fallback. If there is some try/catch missing on the MAIN thread we will handle it here.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <remarks>Gives the option to try and continue anyway.</remarks>
		public static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			DialogResult result = DialogResult.Abort;
			try
			{
				result = MessageBox.Show("Uhh Oh! Please contact the developers "
				  + "with the following information:\n\n" + e.Exception.Message
				  + e.Exception.StackTrace, "Application Error",
				  MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
			}
			finally
			{
				if (result == DialogResult.Abort)
				{
					Application.Exit();
				}
			}
		}


		/// <summary>
		/// Fallback. If there is some try/catch missing in any thread we will handle it here, just before the application quits unhandled
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <remarks>If we hit this, there is no way to recover.</remarks>
		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			//if (e.ExceptionObject is Exception)
			//{
			//	// TODO: Do something
			//}

			try
			{
				Exception ex = (Exception)e.ExceptionObject;

				MessageBox.Show("Uhh Oh! Please contact the developers with the following"
					  + " information:\n\n" + ex.Message + ex.StackTrace,
					  "Fatal unhandled Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}
			finally
			{
				Application.Exit();
			}

		}
	}
}
