﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WBrowser
{
	/// <summary>
	/// Scrapes the source of a web page returning a list of matches.
	/// </summary>
	public class Scraper
	{
		/// <summary>
		/// Represents an individual match results.
		/// </summary>
		public struct ScrapedItem
		{
			/// <summary>
			/// 
			/// </summary>
			public string RootURL;
			/// <summary>
			/// 
			/// </summary>
			public string Data;
			/// <summary>
			/// 
			/// </summary>
			public string Text;
			/// <summary>
			/// Fully qualified URL.
			/// </summary>
			public string FQU;
		}

		/// <summary>
		/// Determines what we are scraping for.
		/// </summary>
		public enum ScraperType
		{
			/// <summary>
			/// Hyperlinks
			/// </summary>
			Links,
			/// <summary>
			/// Images
			/// </summary>
			Images
		}

		// default to link extraction
		private string whattoMatchRegEx		= @"(<a.*?>.*?</a>)";
		private string attributeMatchRegEx	= @"href=\""(.*?)\""";
		private string innerTagMatchRegEx	= @"\s*<.*?>\s*";

		///// <summary>
		///// The RegEx expression to match what we're looking for.
		///// </summary>
		//public string WhatToMatchRegEx
		//{
		//	get
		//	{
		//		return whattoMatchRegEx;
		//	}

		//	set
		//	{
		//		whattoMatchRegEx = value;
		//	}
		//}

		///// <summary>
		///// 
		///// </summary>
		//public string AttributeMatchRegEx
		//{
		//	get
		//	{
		//		return attributeMatchRegEx;
		//	}

		//	set
		//	{
		//		attributeMatchRegEx = value;
		//	}
		//}

		///// <summary>
		///// 
		///// </summary>
		//public string InnerTagMatchRegEx
		//{
		//	get
		//	{
		//		return innerTagMatchRegEx;
		//	}

		//	set
		//	{
		//		innerTagMatchRegEx = value;
		//	}
		//}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="rooturl"></param>
		/// <param name="source">The source HTML that we want to parse.</param>
		/// <param name="st"></param>
		/// <returns>A list of matches.</returns>
		/// <remarks>This is where we add different scrape definitions.</remarks>
		public List<ScrapedItem> Find(string rooturl, string source, ScraperType st)
		{
			// what are we scraping for?
			switch (st)
			{
				case ScraperType.Links:
					whattoMatchRegEx	= @"(<a.*?>.*?</a>)";
					attributeMatchRegEx = @"href=\""(.*?)\""";
					innerTagMatchRegEx	= @"\s*<.*?>\s*";
					break;

				case ScraperType.Images:
					whattoMatchRegEx	= @"(<img.*?>.*?</a>)";
					attributeMatchRegEx = @"src=\""(.*?)\""";
					innerTagMatchRegEx	= @"\s*<.*?>\s*";
					break;

				default:	// links
					whattoMatchRegEx	= @"(<a.*?>.*?</a>)";
					attributeMatchRegEx = @"href=\""(.*?)\""";
					innerTagMatchRegEx	= @"\s*<.*?>\s*";
					break;
			}

			List<ScrapedItem> list = new List<ScrapedItem>();

			// Find all the matches in source
			MatchCollection allmatches = Regex.Matches(source, whattoMatchRegEx, RegexOptions.Singleline);

			// Loop over each match
			foreach (Match singlematch in allmatches)
			{
				string value = singlematch.Groups[1].Value;
				ScrapedItem si = new ScrapedItem();

				// Get the attribute
				Match attribute = Regex.Match(value, attributeMatchRegEx, RegexOptions.Singleline);
				if (attribute.Success)
				{
					si.Data = attribute.Groups[1].Value;

					// default to the initial link
					si.FQU = si.Data;

					if (si.Data.StartsWith("//"))
					{
						si.FQU = si.Data.Substring(2);
					}
					else if (si.Data.StartsWith("/"))	// it's a relative URL
					{
						si.FQU = rooturl + si.Data.Substring(1);
					}
				}

				// Remove inner tags from text
				string innertext = Regex.Replace(value, innerTagMatchRegEx, "", RegexOptions.Singleline);
				si.Text = innertext;

				list.Add(si);
			}
			return list;
		}

	}

}
