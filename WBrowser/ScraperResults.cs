﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace WBrowser
{
	/// <summary>
	/// 
	/// </summary>
	public partial class ScraperResults : Form
	{
		private string _rooturl = "";
		private string _source = "";
		private Scraper.ScraperType _scrapertype = Scraper.ScraperType.Links;
		private Scraper _scraper;

		/// <summary>
		/// CStor.
		/// </summary>
		internal ScraperResults()
		{
			InitializeComponent();
		}

		/// <summary>
		/// CStor.
		/// </summary>
		/// <param name="rooturl">The root URL used for changing relative links, images etc to a fully qualified URL.</param>
		/// <param name="source">The HTML source of the page to scrape.</param>
		/// <param name="st">What we're scraping for.</param>
		public ScraperResults(string rooturl, string source, Scraper.ScraperType st) : this()
		{
			_rooturl = rooturl;
			_source = source;
			_scrapertype = st;
		}

		private void ScraperResults_Load(object sender, EventArgs e)
		{
			_scraper = new Scraper();
			//_scraper.Find(_source, _scrapertype);

			lvwResults.Items.Clear();

			List<Scraper.ScrapedItem> list = new List<Scraper.ScrapedItem>();
			list = _scraper.Find(_rooturl, _source, _scrapertype);

			foreach (Scraper.ScrapedItem i in list)
			{
				//Add items in the listview
				string[] arr = new string[4];
				ListViewItem itm ;

				//Add first item
				arr[0] = "";
				arr[1] = i.Text;
				arr[2] = i.Data;
				arr[3] = i.FQU;
				itm = new ListViewItem(arr);

				itm.Checked = false;
				
				lvwResults.Items.Add(itm);
			}
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			string ver = (new WebBrowser()).Version.ToString();

			return;

			foreach (ListViewItem lvi in lvwResults.CheckedItems)
			{
				//Console.WriteLine(lvi.SubItems["chData"].Text);
				Console.WriteLine(lvi.SubItems[0].Text);
			}
		}


		#region OwnerDraw the ListView so we can use a Select/Deselect All checkboxes in the first column

		bool clicked = false;
		CheckBoxState state;

		private void lvwResults_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
		{
			TextFormatFlags flags = TextFormatFlags.LeftAndRightPadding;
			e.DrawBackground();
			CheckBoxRenderer.DrawCheckBox(e.Graphics, ClientRectangle.Location, state);
			e.DrawText(flags);
		}

		private void lvwResults_DrawItem(object sender, DrawListViewItemEventArgs e)
		{
			e.DrawDefault = true;
		}

		private void lvwResults_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
		{
			e.DrawDefault = true;
		}

		private void lvwResults_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			// ignore if not the checkbox column
			if (e.Column != 0)
				return;

			if (!clicked)
			{
				clicked = true;
				state = CheckBoxState.CheckedPressed;

				foreach (ListViewItem item in lvwResults.Items)
				{
					item.Checked = true;
				}

				Invalidate();
			}
			else
			{
				clicked = false;
				state = CheckBoxState.UncheckedNormal;
				Invalidate();

				foreach (ListViewItem item in lvwResults.Items)
				{
					item.Checked = false;
				}
			}
		}

		#endregion


	}   // end partial class ScraperResults : Form

}   // end namespace WBrowser
